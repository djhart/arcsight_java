package com.edgetech.arcquery;

import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import com.arcsight.coma.model.client.ListWrapper;
import com.arcsight.product.core.service.v1.client.ws.LoginServiceClientFactory;
import com.arcsight.product.core.service.v1.client.ws.api.LoginService;
import com.arcsight.product.manager.resource.service.v1.client.ws.QueryViewerServiceClientFactory;
import com.arcsight.product.manager.resource.service.v1.client.ws.ResourceServiceClientFactory;
import com.arcsight.product.manager.resource.service.v1.client.ws.api.QueryViewerService;
import com.arcsight.product.manager.resource.service.v1.client.ws.api.ResourceService;
import com.arcsight.product.manager.resource.service.v1.model.Resource;
import com.arcsight.product.manager.resource.service.v1.model.activelist.ActiveListEntry;
import com.arcsight.product.manager.resource.service.v1.model.activelist.ActiveListEntryList;
import com.arcsight.product.manager.resource.service.v1.model.dashboard.MatrixData;

public class QueryTest {
	
	// Static Block
	static {
	HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			// Make sure that hostname is valid
			return true; }
	}); }


	public static void main(String[] args) {

		// Set the Base URL
		System.setProperty("com.arcsight.coma.client.ws.baseURL", "https://sol-arcsight:8443/www/");
		// Get the LoginService and login
		LoginServiceClientFactory factoryL = new LoginServiceClientFactory();
		LoginService serviceL = factoryL.createClient();
		String authToken = serviceL.login(null, "admin", "arcsight");

		// Get the QueryViewerService and get the data
		
		QueryViewerServiceClientFactory factory = new QueryViewerServiceClientFactory (); 
		QueryViewerService service = factory.createClient();
		MatrixData md = service.getMatrixData(authToken, "c0azgwCcBABCdKpDO7eweyA==");
		
		
		// Get the Column Names
		List<String> headers = md.getColumnHeaders(); 
		int col=0;
		for (String header : headers) {
		System.out.printf((col++%2 == 0 ? "%60s" : "%20s\n"), header); }
		// Get the Data
		List<ListWrapper> rows = md.getRows(); 
		for (ListWrapper row : rows) {
		List value = row.getValue(); for (Object obj : value) {
		System.out.printf((col++%2 == 0 ? "%60s" : "%20s\n"), obj); }
		}
		
		

	}

}
