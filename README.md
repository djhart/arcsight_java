# README #

Just a place to store Arcsight Java API code

### What is this repository for? ###

Testing the ability to access basic Arcsight components using the Java API.  Current examples show simple way to

* get authorization token
* retrieve a list of all QueryViewer resourceIds
* integrate of the list of ids to get resource details
* use a resource id to retrieve a query viewer result.